﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ScanHeadsman")]
[assembly: AssemblyDescription("Extract scan related information from Thermo RAW files and save it in CSV format.\nUsing RawFileReader reading tool. Copyright © 2016 by Thermo Fisher Scientific, Inc. All rights reserved")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SDU")]
[assembly: AssemblyProduct("ScanHeadsman")]
[assembly: AssemblyCopyright("Copyright © SDU 2016-2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("137ed7a7-6235-44f5-8b31-0e9e403f5105")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.3.*")]
// [assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.2.0.0")]
