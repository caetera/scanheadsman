﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ThermoFisher.CommonCore.Data.Interfaces;

namespace ScanHeadsman
{
    public struct FragmentationEvent
    {
        public double SelectedMass;
        public string Activation;
        public double Energy;
    }

    public struct InstrumentMethod
    {
        public string Name;
        public string Text;
    }

    public static class ThermoRawfileHelpers : object
    {
        public static List<InstrumentMethod> GetMethods(IRawDataPlus rawfile)
        {
            int numMethods = rawfile.InstrumentMethodsCount;
            string[] methodNames = rawfile.GetAllInstrumentFriendlyNamesFromInstrumentMethod();

            List<InstrumentMethod> methods = new List<InstrumentMethod>(numMethods);

            for (int n = 0; n < numMethods; n++)
            {
                methods.Add(new InstrumentMethod{Name = methodNames[n], Text = rawfile.GetInstrumentMethod(n)});
            }

            return methods;
        }

        public static IList<FragmentationEvent> ExtractFragmentationData(string scanFilter)
        {
            List<FragmentationEvent> fragmentationData = new List<FragmentationEvent>();
            Regex fEventExpr = new Regex(@"\S+@\S+");
            Regex activationExpr = new Regex(@"(\D+)([0-9.]+)");

            foreach (Match group in fEventExpr.Matches(scanFilter))
                //group is something like <number>@<letters><number>(the part with @ can repeat many times)
            {
                string[] parts = group.Value.Split('@');
                FragmentationEvent fEvent = new FragmentationEvent();
                fEvent.SelectedMass = Double.Parse(parts[0]);//first part is selected mass
                for (int i = 1; i < parts.Length; i++)//second part and further is activations
                {
                    Match m = activationExpr.Match(parts[i]);//each part is something like hcd5.00 
                    fEvent.Activation = m.Groups[1].Value.ToUpper();
                    fEvent.Energy = Double.Parse(m.Groups[2].Value);
                    fragmentationData.Add(fEvent);
                    fEvent = new FragmentationEvent();//if we have multiple fragmentation add new event with selected mass 0
                    fEvent.SelectedMass = 0;
                }
                
            }

            return fragmentationData;
        }

        public static Dictionary<string, string> ParseScanEvent (string scanEvent)
        {
            Dictionary<string, string> parsedEvent = new Dictionary<string, string>();

            foreach (string item in scanEvent.Split())
            {
                switch (item)
                {
                    case "+": parsedEvent["Polarity"] = "Positive"; break;
                    case "-": parsedEvent["Polarity"] = "Negative"; break;
                    case "Full": parsedEvent["ScanType"] = "Full"; break;
                    case "Z": parsedEvent["ScanType"] = "Zoom"; break;
                    case "SIM": parsedEvent["ScanType"] = "SIM"; break;
                    case "SRM": parsedEvent["ScanType"] = "SRM"; break;
                    case "CRM": parsedEvent["ScanType"] = "CRM"; break;
                    case "Q1MS": parsedEvent["ScanType"] = "Q1MS"; break;
                    case "Q3MS": parsedEvent["ScanType"] = "Q3MS"; break;
                    case "ITMS": parsedEvent["Analyzer"] = "ITMS"; break;
                    case "FTMS": parsedEvent["Analyzer"] = "FTMS"; break;
                    case "TQMS": parsedEvent["Analyzer"] = "TQMS"; break;
                    case "SQMS": parsedEvent["Analyzer"] = "SQMS"; break;
                    case "TOFMS": parsedEvent["Analyzer"] = "TOFMS"; break;
                    case "Sector": parsedEvent["Analyzer"] = "Sector"; break;
                    case "APCI": parsedEvent["Ionization"] = "APCI"; break;
                    case "ESI": parsedEvent["Ionization"] = "ESI"; break;
                    case "EI": parsedEvent["Ionization"] = "EI"; break;
                    case "CI": parsedEvent["Ionization"] = "CI"; break;
                    case "NSI": parsedEvent["Ionization"] = "NSI"; break;
                    case "FAB": parsedEvent["Ionization"] = "FAB"; break;
                    case "TSP": parsedEvent["Ionization"] = "TSP"; break;
                    case "FD": parsedEvent["Ionization"] = "FD"; break;
                    case "MALDI": parsedEvent["Ionization"] = "MALDI"; break;
                    case "GD": parsedEvent["Ionization"] = "GD"; break;
                    default:
                        {
                            if (item.StartsWith("!"))
                            {
                                try
                                {
                                parsedEvent[ScanEventCode[item.Substring(1)]] = "Off";
                                }
                                catch
                                {
                                    //nothing
                                }
                            }
                            else
                            {
                                try
                                {
                                    parsedEvent[ScanEventCode[item]] = "On";
                                }
                                catch
                                {
                                    //nothing
                                }
                            }
                            break;
                        }
                }
                
            }

            return parsedEvent;
        }

        private static Dictionary<string, string> ScanEventCode = new Dictionary<string,string>
        {
            {"d", "DependentScan"},
            {"t", "TurboScan"},
            {"sid", "Source CID"},
            {"pi", "PhotoIonization"},
            {"cv", "CompensationVoltage"},
            {"det", "DetectorValid"},
            {"E", "Enhanced"},
            {"w", "Wideband"},
            {"sa", "SupplementalActivation"},
            {"msa", "MultistateActivation"},
            {"corona", "Corona"},
            {"AM", "AccurateMass"},
            {"AMI", "AccurateMass"},
            {"u", "UltraScan"},
            {"BSCAN", "BSectorScan"},
            {"ESCAN", "ESectorScan"},
            {"lock", "LockMass"},
            {"msx", "Multiplex"},
            {"nptr", "NPTR"}
        };
    }
}
