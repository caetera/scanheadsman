﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ScanHeadsman.Helpers
{
    public class Table
    {
        public string[] Headers { get; set; }

        public List<Dictionary<string, string>> Data { get; set; }

        public Table(List<string> lines)
        {
            Data = new List<Dictionary<string, string>>();

            if (lines.Count > 1) //ignore empty lists
            {
                Headers = lines[0].Split(new char[] { '|' }).Where(s => s.Trim() != "").
                    Select(s => s.Trim().ToLower().Replace(" ", "_")).ToArray();
                //ignore empty headers, convert all to lowercase and remove whitespaces

                foreach (string line in lines.Skip(1))//skip header and parse the table
                {
                    string[] values = line.Split(new char[] { '|' });
                    Dictionary<string, string> row = new Dictionary<string, string>(Headers.Length);
                    for (int i = 0;  i < Headers.Length; i++)
                    {
                        row[Headers[i]] = values[i].Trim(); 
                    }    

                    Data.Add(row);
                }
            }
        }

        public List<T> ToList<T>()
        {
            List<T> result = new List<T>();

            foreach (Dictionary<string, string> row in Data)
            {
                MethodInfo method = typeof(T).GetMethod("FromDict"); //it is not pretty
                if (method != null) result.Add((T)method.Invoke(null, new object[] { row }));
                else throw new Exception("Object has to implement FromDict");
            }

            return result;
        }
    }
}
