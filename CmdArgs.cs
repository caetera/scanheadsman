﻿
namespace ScanHeadsman
{
    public class CmdArgs
    {
        public string detail;
        public int? maxthreads;
        public string methods;
        public string input;
        public string output;
        public bool? summary;
    }
}
