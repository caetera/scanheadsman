# -*- coding: utf-8 -*-
"""
Created on Sun Nov 18 18:59:50 2018

@author: vgor
"""

from pandas import read_csv
from subprocess import check_call, CalledProcessError
from os import path, mkdir
from statsmodels.nonparametric.smoothers_lowess import lowess
from scipy.stats import binned_statistic
from shutil import which
from platform import system
import pylab as plt
import numpy as np
import sys

##Path to ScanHeadsman
##Change it to fit
shPath = "./ScanHeadsman.exe"


def process(filename, outLocation):
    
    df = read_csv(filename)
        
    #min and max intensity
    df["minInt"] = df["minInt"].apply(np.log10)
    df["maxInt"] = df["maxInt"].apply(np.log10)
    df["meanInt"] = df["meanInt"].apply(np.log10)
    
    #split MS1 and MS2 scans
    df1 = df[df["MSOrder"] == 1]    
    df2 = df[df["MSOrder"] > 1]
    
    if df1.shape[0] > 0:
        plt.figure("IIT-MS1 at RT", edgecolor = "w", facecolor = "w", figsize = (10, 6), dpi = 100)
        plt.title("IIT-MS1 vs RT [all time]")
        plt.scatter(df.loc[df["MSOrder"] == 1, "RT [min]"].values,
                df.loc[df["MSOrder"] == 1, "Ion Injection Time (ms)"].values, alpha = 0.7)
        lowessFit = lowess(df.loc[df["MSOrder"] == 1, "Ion Injection Time (ms)"].values,
                            df.loc[df["MSOrder"] == 1, "RT [min]"].values, is_sorted=True, frac=0.02, it=0)
        plt.plot(lowessFit[:, 0], lowessFit[:, 1], "r-", lw = 3, label = "Average IIT")
        plt.xlabel("Retention Time (min)")
        plt.ylabel("MS1 ion injection time (ms)")
        plt.ylim(bottom = -1)
        plt.xlim(left = 0, right = max(df.loc[:, "RT [min]"].values))
        plt.legend()
        plt.savefig(path.join(outLocation, "IITMS1.png"))
        plt.close()
        
        plt.figure("BasePeak Chromatogram", edgecolor = "w", facecolor = "w", figsize = (10, 6), dpi = 100)
        plt.title("BPI in MS1 [all time]")
        lowessFit = lowess(df.loc[df["MSOrder"] == 1, "BasePeakIntensity"].values,
                            df.loc[df["MSOrder"] == 1, "RT [min]"].values, is_sorted=True, frac=0.02, it=0)
        plt.plot(lowessFit[:, 0], lowessFit[:, 1], "r-", lw = 3, label = "BPI")
        plt.xlabel("Retention Time (min)")
        plt.ylabel("BasePeakIntensity")
        plt.ylim(bottom = -1)
        plt.xlim(left = 0, right = max(df.loc[:, "RT [min]"].values))
        plt.legend()
        plt.savefig(path.join(outLocation, "BPI.png"))
        plt.close()

        plt.figure("Cycle Time vs RT", edgecolor = "w", facecolor = "w", figsize = (10, 6), dpi = 100)
        plt.title("Cycle Time vs RT [all time]")
        ms1times = df.loc[df["MSOrder"] == 1, u"RT [min]"].values
        plt.scatter(ms1times[:-1], 60. * (ms1times[1:] - ms1times[:-1]), s = 5, alpha = 0.7)
        lowessFit = lowess(60. *(ms1times[1:] - ms1times[:-1]), ms1times[:-1], is_sorted=True, frac=0.025, it=0)
        plt.plot(lowessFit[:, 0], lowessFit[:, 1], "r-", lw = 3, label = "Average Cycle Time")
        plt.xlabel("RT (min)")
        plt.ylabel("Cycle Time (s)")
        plt.legend()
        plt.ylim(bottom = 0)
        plt.xlim((ms1times[0], ms1times[-1]))
        plt.savefig(path.join(outLocation, "CycleTimeRT.png"))
        plt.close()
    
    if df2.shape[0] > 0:
        plt.figure("Charge", edgecolor = "w", facecolor = "w", figsize = (10, 6), dpi = 100)
        plt.title("Charge state")
        z = df.loc[df["MSOrder"] == 2, "Charge State"].values
        plt.hist(z, bins = range(int(min(z)), int(max(z)) + 2), align = "left", rwidth = 0.8)
        plt.xlabel("Charge state")
        plt.savefig(path.join(outLocation, "ChargeStates.png"))
        plt.close()
        
        
        plt.figure("Dynamic Range", edgecolor = "w", facecolor = "w", figsize = (10, 6), dpi = 100)
        plt.title("Dynamic Range")
        plt.scatter(df1["meanInt"], df1["maxInt"] - df1["minInt"], s = 5, alpha = 0.3, color = "c", label = "MS1 scans")
        plt.scatter(df2["meanInt"], df2["maxInt"] - df2["minInt"], s = 5, alpha = 0.3, color = "g", label = "MSn scans")
        avgMin = binned_statistic(df["meanInt"], df["minInt"], bins = 100)[0]
        avgMax, binEdges, binNumber = binned_statistic(df["meanInt"], df["maxInt"], bins = 100)
        avgMean = (binEdges[1:] + binEdges[:-1])/2
        plt.plot(avgMean, avgMax - avgMin, "k-", lw = 3, label = "Dynamic range")
        plt.xlabel("log10(Intensity)")
        plt.ylabel("log10(Max/Min)")
        plt.ylim(bottom = 0)
        plt.legend(loc = "upper left")
        plt.savefig(path.join(outLocation, "DynamicRange.png"))
        plt.close()
        

        avTopN = 1.0 * sum(df["MSOrder"] != 1)/sum(df["MSOrder"] == 1)
        
        plt.figure("TopN vs RT", edgecolor = "w", facecolor = "w", figsize = (10, 6), dpi = 100)
        plt.title("TopN vs RT [all time]")
        ms1scans = df.loc[df["MSOrder"] == 1, u"Scan"].values
        plt.scatter(ms1times[:-1], (ms1scans[1:] - ms1scans[:-1]) - 1, s = 5, alpha = 0.7)
        lowessFit = lowess((ms1scans[1:] - ms1scans[:-1]) - 1, ms1times[:-1], is_sorted=True, frac=0.025, it=0)
        plt.plot(lowessFit[:, 0], lowessFit[:, 1], "r-", lw = 3, label = "Average TopN")
        plt.axhline(avTopN, color = "c", lw = 3, label = "Mean TopN")
        plt.text(ms1times[-1]*0.99, avTopN*1.04, "{:.3f}".format(avTopN), ha = "right", color = "c", fontsize = 14)
        plt.xlabel("RT (min)")
        plt.ylabel("TopN")
        plt.legend()
        plt.ylim(bottom = -1)
        plt.xlim((ms1times[0], ms1times[-1]))
        plt.savefig(path.join(outLocation, "TopNRT.png"))
        plt.close()


        plt.figure("TopN vs Cycle Time", edgecolor = "w", facecolor = "w", figsize = (10, 6), dpi = 100)
        plt.title("TopN vs Cycle time [all time]")
        plt.scatter(60. * (ms1times[1:] - ms1times[:-1]), (ms1scans[1:] - ms1scans[:-1]) - 1, s = 5, alpha = 0.7)
        plt.xlabel("Cycle Time (sec)")
        plt.ylabel("TopN")
        plt.ylim(bottom = -0.3)
        plt.xlim(left = 0)
        plt.savefig(path.join(outLocation, "TopNCycleTime.png"))
        plt.close()
        
        
        plt.figure("IIT-MS2 at RT", edgecolor = "w", facecolor = "w", figsize = (10, 6), dpi = 100)
        plt.title("IIT-MS2 vs RT [all time]")
        plt.scatter(df.loc[df["MSOrder"] == 2, "RT [min]"].values,
                df.loc[df["MSOrder"] == 2, "Ion Injection Time (ms)"].values, alpha = 0.7)
        lowessFit = lowess(df.loc[df["MSOrder"] == 2, "Ion Injection Time (ms)"].values,
                            df.loc[df["MSOrder"] == 2, "RT [min]"].values, is_sorted=True, frac=0.02, it=0)
        plt.plot(lowessFit[:, 0], lowessFit[:, 1], "r-", lw = 3, label = "Average IIT")
        plt.xlabel("Retention Time (min)")
        plt.ylabel("MS2 ion injection time (ms)")
        plt.ylim(bottom = -1)
        plt.xlim(left = 0, right = max(df.loc[:, "RT [min]"].values))
        plt.legend()
        plt.savefig(path.join(outLocation, "IITMS2.png"))
        plt.close()

if __name__ == "__main__":
    dirname, filename = path.split(sys.argv[1])
    print("Plotting diagnostic plots for {}".format(filename))
    name, ext = path.splitext(filename)
    resdir = path.join(dirname, name)
    
    if (not path.exists(resdir)):
        mkdir(resdir)
    
    print("Results will be stored in {}".format(resdir))
        
    txtPath = path.join(resdir, name + ".csv")
    if (not path.exists(txtPath)):
        print("It seems like ScanHeadsman was not executed yet\nHold on, I'll do it for you")
        execline = [shPath, "-d Small", path.join(dirname, filename), txtPath]
        if not system().lower() == "windows":
            if which("mono") is None:
                print("Mono is required to parse RAW files on your platform, please, install it")
                sys.exit()
            else:
                execline.insert(0, "mono")
        try:
            check_call(execline)
        except Exception as ex:
            if type(ex) is CalledProcessError:
                print("Error running ScanHeadsman\n{} exited with error code {}".format(" ".join(ex.cmd), ex.returncode))
            else:
                print("Error\n{}".format(ex))
                
            sys.exit()
            
    else:
        print("""It seems like ScanHeadsman was executed already\nI will use the old export file.""")
                
    process(txtPath, resdir)
