# ScanHeadsman
## Description
ScanHeadsman is a tool to extract scan data and methods from Thermo RAW file into a text-based format.

For each scan it can export scan meta data, for example, type of scan, isolation window, injection time, scan headers etc, and centroied MS data.
It aims to extract as much data as available in the RAW file.

It can extract methods from RAW files in plain text format (all instruments) and MS methods in XML/CSV formats for RAW files generated on TNG-based platform (Tribrids and Exploris).
Please, see the details below.

## Requirements
On Windows **.NET 4.7 Runtime** or later has to be installed in the system. You can download it from [Microsoft](https://dotnet.microsoft.com/download/dotnet-framework/net47),
however, it is preinstalled in all modern versions of Windows. **ScanHeadsman** runs over [Mono](https://www.mono-project.com/download/stable/) on other platforms.

## License
This software is using **RawFileReader reading tool**. Copyright � 2016 by Thermo Fisher Scientific, Inc. All rights reserved
**RawFileReader reading tool** (.NET assemblies) is distributed under RawFileReader license (see RawFileReaderLicense.md)

**ScanHeadsman** is provided under MIT license (see License.md)

## Usage
Just invoke it from the console with the following arguments.

`ScanHeadsman.exe [options] [Input Path] [Output Path]`

All arguments are optional, input and output can be provided either as option
arguments (`-i|input`, `-o|output`) or as positional arguments

If no input/output is provided all **RAW** files in the current folder will be processed.

If the provided _Input Path_ is a folder, all **RAW** files in that folder will be processed, if it is a single **RAW** file it will be processed individually.

Extracted information is written in a **CSV**-formatted (commas as separators, ASCII encoded) text file.
By default the name of the file is the same as the name of **RAW** file with the extension changed to **.csv**
(i.e. *somerawfile.raw* becomes *somerawfile.csv*). You can override the default name of the export file by providing _Output Path_ 
(for single **RAW** it has to be path to the file, for multiple **RAW** files it has to be valid folder).

Options:

`-h`, `--help`               Usage information

`-v`, `--version`            Show version information

`-i`, `--input=VALUE`        Input file/folder

`-o`, `--output=VALUE`       Output file/folder

`-d`, `--detail=VALUE`       MS data detail level, i.e. the amount of data written to the output
the value for this option has to be one of the following

   * `None` or `0`,   no MS data is produced, this option can be used when only summary or method information is necessary.
   * `Header` or `1`, MS data includes only scan headers. (Default value)
   * `Small` or `2`,  Same as above, MS scan data is provided as aggregated values (minimum, maximum, and mean intensities).
   * `Full` or `3`,  Same as above, full centroided MS scan data is provided - m/z values and intensities are exported as two columns of space separated float numbers (centroided data is used). **WARNING** expect really large csv file if it contains full MS data.

`-m`, `--methods[=VALUE]`    Extract method information, this option supports additional modifiers,
that are provieded after a semicolon, or equal sign, i.e. **--methods:MODIFIER** The list of modifiers is below

   * `text`, no modifier or default - **--methods:text** or **--methods** - Methods exported in the plain text file. The text file is Unicode-encoded and created with the same filename and location as the **RAW** file with the extension changed to **methods.txt** (i.e. *somerawfile.raw* becomes *somerawfile.methods.txt*).
   * `xml` - **--methods:xml** - **ONLY** the first MS methods is exported in the XML formatted file. **ONLY TNG-Calcium** methods are supported, i.e. Fusion series. The XML file is Unicode-encoded and created with the same filename and location as the **RAW** file with the extension changed to **method.xml** (i.e. *somerawfile.raw* becomes *somerawfile.method.xml*).
   * `csv` - **--methods:csv** - **ONLY** MS experiments in the first MS methods are exported in the CSV formatted file. **ONLY TNG-Calcium** methods are supported, i.e. Fusion series. The CSV file is ASCII-encoded, with commas as column separators and created with the same filename and location as the **RAW** file with the extension changed to **experiments.csv** (i.e. *somerawfile.raw* becomes *somerawfile.experiments.csv*).

`-u, --summary`            produce a file with brief infromation about the file and the instrument 
(creation date, instrument model, software version, etc) in **CSV**-format. The file will be saved as *somerawfile.sum.csv*.

`-t, --threads=VALUE`      (expert use) change the number of parallel _working_ threads used for each **RAW** file. The value should be positive integer
or -1 (no limit). Valid examples are `--threads:10` or `--threads:-1`. Seting this value too high might lead to performance issues.

Any existing files will be overwritten silently, all non-existing folders will be created.

## Examples

`ScanHeadsman.exe`

Process all **RAW** files in the current folder with header only MS data

`ScanHeadsman.exe D:\RawData\OpearationY`

Process all **RAW** files in *D:\RawData\OperationY* with header-only MS data

`ScanHeadsman.exe D:\RawData\OperationY\storageFacility.raw`

Process *D:\RawData\OperationY\storageFacility.raw* and save the results in *D:\RawData\OperationY\storageFacility.txt* with header-only MS data

`ScanHeadsman.exe -d Full D:\RawData\OperationY\storageFacility.raw`

Process *D:\RawData\OperationY\storageFacility.raw* and save the results in *D:\RawData\OperationY\storageFacility.txt* with full MS data

`ScanHeadsman.exe -d Small D:\RawData\OperationY\storageFacility.raw`

Process *D:\RawData\OperationY\storageFacility.raw* and save the results in *D:\RawData\OperationY\storageFacility.txt* with aggregated MS data

`ScanHeadsman.exe D:\RawData\OperationY\storageFacility.raw D:\RawData\Police\coward.csv`

Process *D:\RawData\OperationY\storageFacility.raw* and save the results in *D:\RawData\Police\coward.csv* with header-only MS data

`ScanHeadsman.exe D:\RawData\OperationY\storageFacility.raw --methods:csv D:\RawData\Police\coward.csv`

Process *D:\RawData\OperationY\storageFacility.raw* and save the results in *D:\RawData\Police\coward.csv* with header-only MS data,
save **TNG-Calcium** experiments into *D:\RawData\OperationY\storageFacility.experiments.csv* 

## Downloads

[Downloads](https://bitbucket.org/caetera/scanheadsman/downloads/) section contains stable releases. Version 0.5 is using MSFileReader.