# ScanHeadsman changelog

### 1.3.20211020

* Removed `-x|--exec` command line option

* Changed Mono.Options to version 6.12.0.148

* Introduced unified MS detail option (`-d|--detail`) instead of `--noms` and `--smallms`; `None` level allows skipping all scan output; default output level changed to scan headers only

* Remote assemblies error is fixed for windows

* Few minor UI fixes

### 1.2.20200730

First version included in changelog