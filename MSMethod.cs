﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using ScanHeadsman.Helpers;

namespace ScanHeadsman
{
    [Serializable()]
    [XmlRoot("TargetedMass")]
    public class TargetedMass
    {
        //A single element of Targeted Mass List
        [XmlAttribute("mz")]
        public double MZ;
        [XmlAttribute("z")]
        public int Z;
        [XmlAttribute("name")]
        public string Name;
        [XmlAttribute("formula")]
        public string Formula;
        [XmlAttribute("adduct")]
        public string Adduct;
        [XmlAttribute("threshold")]
        public double Threshold;

        public TargetedMass()
        {
            //empty constructor for serealization
        }

        public TargetedMass(Dictionary<string, string> data) //parse from string-string dictionary
        {
            string strMZ;
            string strZ;
            string strThreshold;

            data.TryGetValue("compoundname", out Name);
            data.TryGetValue("formula", out Formula);
            data.TryGetValue("adductpositive", out Adduct);
            data.TryGetValue("adductneagtive", out Adduct);
            data.TryGetValue("m/z", out strMZ);
            data.TryGetValue("z", out strZ);
            data.TryGetValue("threshold", out strThreshold);
            double.TryParse(strMZ, out MZ);
            int.TryParse(strZ, out Z);
            double.TryParse(strThreshold, out Threshold);
        }

        public override string ToString()
        {
            return String.Format("(mz={0} z={1} name={2} formula={3} adduct={4} threshold={5})", this.MZ, this.Z, this.Name, this.Formula, this.Adduct, this.Threshold);
        }

        public static TargetedMass FromDict(Dictionary<string, string> dict)
        {
            return new TargetedMass(dict);
        }
    }

    [Serializable()]
    [XmlRoot("RetentionTimeStandards")]
    public class RTStandard
    {
        [XmlAttribute("name")]
        public string PeptideName;
        [XmlAttribute("mz")]
        public double MZ;
        [XmlAttribute("rt")]
        public double RT;
        [XmlAttribute("rt_window")]
        public string Window;

        public RTStandard()
        {
            //empty constructor for serialization
        }

        public RTStandard(Dictionary<string, string> data) //parse from string-string dictionary
        {
            string strMZ;
            string strRT;

            data.TryGetValue("peptide_name", out PeptideName);
            data.TryGetValue("m/z", out strMZ);
            data.TryGetValue("retention_time", out strRT);
            data.TryGetValue("retention_window", out Window);
            double.TryParse(strMZ, out MZ);
            double.TryParse(strRT, out RT);
        }

        public override string ToString()
        {
            return String.Format("(name={0} mz={1} rt={2} rt_window={3})", this.PeptideName, this.MZ, this.RT, this.Window);
        }

        public static RTStandard FromDict(Dictionary<string, string> dict)
        {
            return new RTStandard(dict);
        }
    }

    [Serializable()]
    [XmlRoot("GlobalSettings")]
    public class GlobalMSSettings
    {
        //Global MS settings, i.e. Tune settings
        //Initially all fields are initialized with nulls (string is nullable by default)
        public double? Duration;
        public string IonSource;
        public double? PositiveV;
        public double? NegativeV;
        public double? SweepGas;
        public double? ITT;
        public string APPI;
        public double? SyringeFlow;
        public double? SyringeVolume;
        public double? SyringeDiameter;
        public string PressureMode;
        public int? DefaultCharge;
        public bool? SettingsFromTune;
        public bool? IsLC;
        public string FAIMS;
        public string ApplicationMode;
        public bool? APD;
        public bool? AcquireX;

        public GlobalMSSettings()
        {
            //empty constructor for serealization
        }
    }

    [Serializable()]
    [XmlRoot("MSExperiment")]
    public class MSExperiment
    {
        //MS experiment parameters
        //Initially all fields are initialized with nulls (string is nullable by default)
        public double? StartTime;
        public double? EndTime;
        public double? CycleTime;
        public bool? DoDDAifNoTarget;
        public string ScanType;
        public int? MSLevel;
        public int? LoopCount;
        public int? LoopControl;
        public double? LoopTime;
        public bool? UseWideQuadIsolation;
        public string IsolationMode;
        public bool? MultiplexIons;
        public double? IsolationWindow;
        public string DefineMSXIDs;
        public int? MaxNumberOfMultiplexIons;
        public double? MS2SupplementalCE;
        public string ActivationType;
        public double? CE;
        public bool? SteppedCE;
        public double? SteppedCEValue;
        public bool? MultistageActivation;
        public double? NeutralLossMass;
        public bool? EThcD;
        public bool? UseCalibratedETDParameters;
        public double? ETDReactionTime;
        public int? ETDReagentTarget;
        public int? MaxETDReagentInjTime;
        public bool? ETDSupplementalActivation;
        public double? SupplementalActivationCE;
        public string DetectorType;
        public string Resolution;
        public string MassRange;
        public string ScanRange;
        public string ScanRangeMode;
        public int? MaxInjTime;
        public int? AGCTarget;
        public bool? MaxParallelTime;
        public int? Microscans;
        public double? MS2ActivationQ;
        public double? MS2ActivationTime;
        public double? ActivationQ;
        public double? SLensRF;
        public bool? UseETDInternalCalibration;
        public double? UVPDTime;
        public bool? UseCalibratedUVPDTime;
        public bool? UseCalibratedUVPDTimeMS2;
        public string DataType;
        public string Polarity;
        public bool? SourceFragmentation;
        public int? PointsPerPeak;
        public string NormalizedAGC;
        public string MaxInjTimeType;
        public string Description;
        public string EnhancedResolution;
        public int? PTRTarget;
        public int? PTRReactionTime;
        public string DynamicRT;
        [System.Xml.Serialization.XmlArray("MassListTable")]
        public List<TargetedMass> TargetedMassList;
        [System.Xml.Serialization.XmlArray("RTStandardTable")]
        public List<RTStandard> RTStandardList;

        public MSExperiment()
        {
            //empty constructor for serealization
        }

        public MSExperiment(string text)
        {
            //initilize from the text as it is found in method
            IEnumerable<string> lines = text.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries).AsEnumerable();

            //extract targeted mass table and remove it
            this.TargetedMassList = new Table(lines.SkipWhile(l => l.IndexOf("Mass List Table") == -1).TakeWhile(l => l.IndexOf("End Mass List Table") == -1).Skip(1).ToList())//Ignore first delimiter line for the table
                .ToList<TargetedMass>(); //convert table to list of objects
            lines = lines.TakeWhile(l => l.IndexOf("Mass List Table") == -1).Concat(lines.SkipWhile(l => l.IndexOf("End Mass List Table") == -1).Skip(1));

            //extract RT standards table and remove it
            this.RTStandardList = new Table(lines.SkipWhile(l => l.IndexOf("Retention Time Standards") == -1).TakeWhile(l => l.IndexOf("End Retention Time Standards") == -1).Skip(1).ToList())
                .ToList<RTStandard>();
            lines = lines.TakeWhile(l => l.IndexOf("Retention Time Standards") == -1).Concat(lines.SkipWhile(l => l.IndexOf("End Retention Time Standards") == -1).Skip(1));

            //ignore first line (header), only proceed till the Mass List Table (if any)
            foreach (string line in lines.Skip(1)) 
            {
                string[] value = line.Split(new char[] { '=' });
                switch(value[0].Trim()) //known MS experiment parameters, casting to proper type if necessary
                {
                    case "Start Time (min)": this.StartTime = double.Parse(value[1]); break;
                    case "End Time (min)": this.EndTime = double.Parse(value[1]); break;
                    case "Cycle Time (sec)": this.CycleTime = double.Parse(value[1]); break;
                    case "Do data dependent experiment if no target species are found": this.DoDDAifNoTarget = bool.Parse(value[1]); break;
                    case "MSn Level": this.MSLevel = int.Parse(value[1]); break;
                    case "Loop Count": this.LoopCount = int.Parse(value[1]); break;
                    case "Loop Control": this.LoopControl = int.Parse(value[1]); break;
                    case "Loop Time": this.LoopTime = double.Parse(value[1]); break;
                    case "Isolation Mode": this.IsolationMode = value[1].Trim(); break;
                    case "Multiplex Ions Enabled": this.MultiplexIons = bool.Parse(value[1]); break;
                    case "Isolation Window": this.IsolationWindow = double.Parse(value[1]); break;
                    case "Define MSX IDs": this.DefineMSXIDs = value[1].Trim(); break;
                    case "Maximum number of multiplexed ions": this.MaxNumberOfMultiplexIons = int.Parse(value[1]); break;
                    case "MS2 Supplemental Collision Energy (%)": this.MS2SupplementalCE = double.Parse(value[1]); break;
                    case "ActivationType": this.ActivationType = value[1].Trim(); break;
                    case "Collision Energy (%)": this.CE = double.Parse(value[1]); break;
                    case "Is Stepped Collision Energy On": this.SteppedCE = bool.Parse(value[1]); break;
                    case "Stepped Collision Energy (%)": this.SteppedCEValue = double.Parse(value[1]); break;
                    case "Multistage Activation": this.MultistageActivation = bool.Parse(value[1]); break;
                    case "Neutral Loss Mass": this.NeutralLossMass = double.Parse(value[1]); break;
                    case "Is EThcD Active": this.EThcD = bool.Parse(value[1]); break;
                    case "Use calibrated charge dependent ETD parameters": this.UseCalibratedETDParameters = bool.Parse(value[1]); break;
                    case "ETD Reaction Time (ms)": this.ETDReactionTime = double.Parse(value[1]); break;
                    case "ETD Reagent Target": this.ETDReagentTarget = int.Parse(value[1]); break;
                    case "Maximum ETD Reagent Injection Time (ms)": this.MaxETDReagentInjTime = int.Parse(value[1]); break;
                    case "Max ETD Reagent Injection Time (ms)": this.MaxETDReagentInjTime = int.Parse(value[1]); break;
                    case "ETD Supplemental Activation": this.ETDSupplementalActivation = bool.Parse(value[1]); break;
                    case "SA Collision Energy (%)": this.SupplementalActivationCE = double.Parse(value[1]); break;
                    case "Use Wide Quad Isolation": this.UseWideQuadIsolation = bool.Parse(value[1]); break;
                    case "Detector Type": this.DetectorType = value[1].Trim(); break;
                    case "Orbitrap Resolution": this.Resolution = value[1].Trim(); break;
                    case "Mass Range": this.MassRange = value[1].Trim(); break;
                    case "Scan Range (m/z)": this.ScanRange = value[1].Trim(); break;
                    case "Scan Range Mode": this.ScanRangeMode = value[1].Trim(); break;    
                    case "Maximum Injection Time (ms)": this.MaxInjTime = int.Parse(value[1]); break;
                    case "Maximum Injection Time Type": this.MaxInjTimeType = value[1].Trim(); break;
                    case "AGC Target": this.AGCTarget = int.Parse(value[1]); break;
                    case "Normalized AGC Target": this.NormalizedAGC = value[1].Trim(); break;
                    case "Inject ions for all available parallelizable time": this.MaxParallelTime = bool.Parse(value[1]); break;
                    case "Microscans": this.Microscans = int.Parse(value[1]); break;
                    case "S-Lens RF Level": this.SLensRF = double.Parse(value[1]); break;
                    case "RF Lens (%)": this.SLensRF = double.Parse(value[1]); break;
                    case "MS2 Activation Q": this.MS2ActivationQ = double.Parse(value[1]); break;
                    case "Activation Q": this.ActivationQ = double.Parse(value[1]); break;
                    case "MS2 CID Activation Time (ms)": this.MS2ActivationTime = int.Parse(value[1]); break;
                    case "Activation Time (ms)": this.MS2ActivationTime = int.Parse(value[1]); break;
                    case "Use ETD Internal Calibration": this.UseETDInternalCalibration = bool.Parse(value[1]); break;
                    case "DataType": this.DataType = value[1].Trim(); break;
                    case "Polarity": this.Polarity = value[1].Trim(); break;
                    case "Source Fragmentation": this.SourceFragmentation = bool.Parse(value[1]); break;
                    case "UVPD Activation Time": this.UVPDTime = double.Parse(value[1]); break;
                    case "Use calibrated molecular weight dependent UVPD Activation Time": this.UseCalibratedUVPDTime = bool.Parse(value[1]); break;
                    case "MS2 Use calibrated molecular weight dependent UVPD Activation Time": this.UseCalibratedUVPDTimeMS2 = bool.Parse(value[1]); break;
                    case "Desired minimum points across the peak": this.PointsPerPeak = int.Parse(value[1]); break;
                    case "Scan Description": this.Description = value[1].Trim(); break;
                    case "Enhanced Resolution Mode": this.EnhancedResolution = value[1].Trim(); break;
                    case "MS2 PTR Reagent Target": this.PTRTarget = int.Parse(value[1]); break;
                    case "MS2 PTR Reaction Time (ms)": this.PTRReactionTime = int.Parse(value[1]); break;
                    case "Dynamic RT": this.DynamicRT = value[1].Trim(); break;
                    default:
                        {
                            if (value[0].Trim().StartsWith("Scan")) this.ScanType = value[0].Trim().Split()[1]; //scan type doesn't follow Item=Value format
                            else if (value[0].Trim() != String.Empty) Console.WriteLine(String.Format("WARNING: Unknown parameter - {0}", value[0])); //ignore all whitespace lines and warn user if something new found
                        } break;
                }
            }

        }

        public bool ShouldSerializeTargetedMassList()
        {
            //do not serialize it if list is empty
            return this.TargetedMassList.Count > 0;
        }

        public bool ShouldSerializeRtStandardList()
        {
            //do not serialize it if list is empty
            return this.RTStandardList.Count > 0;
        }
    }

    [Serializable()]
    [XmlRoot("InstrumentMethod")]
    public class MSMethod
    {
        //Complete MS method
        [XmlAttribute("creator")]
        public string Creator;
        [XmlAttribute("modified")]
        public string LastModified;
        [XmlAttribute("instrument")]
        public string Instrument;
        public GlobalMSSettings GlobalSettings;
        [System.Xml.Serialization.XmlArray("Experiments")]
        public List<MSExperiment> Experiments;
        
        public MSMethod()
        {
            this.Experiments = new List<MSExperiment>();
            this.GlobalSettings = new GlobalMSSettings();
        }

        public MSMethod(string text)
        {
            //intialize from text representation as in method file
            this.Experiments = new List<MSExperiment>();
            this.GlobalSettings = new GlobalMSSettings();

            
            string[] parts = System.Text.RegularExpressions.Regex.Split(text, "Experiment \\d+"); //Split experiments

            foreach (string line in parts[0].Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries)) //Part 0 contains description and global parameters
            {
                string[] value = line.Split(new char[] { '=' }); //Update Global Parameters using Item=Value
                switch (value[0].Trim())
                {
                    case "Use Ion Source Setting from Tune": this.GlobalSettings.SettingsFromTune = bool.Parse(value[1]); break;
                    case "Method Duration (min)": this.GlobalSettings.Duration = double.Parse(value[1]); break;
                    case "Ion Source Type": this.GlobalSettings.IonSource = value[1].Trim(); break;
                    case "Spray Voltage: Positive Ion (V)": this.GlobalSettings.PositiveV = double.Parse(value[1]); break;
                    case "Spray Voltage: Negative Ion (V)": this.GlobalSettings.NegativeV = double.Parse(value[1]); break;
                    case "Sweep Gas (Arb)": this.GlobalSettings.SweepGas = double.Parse(value[1]); break;
                    case "Ion Transfer Tube Temp (°C)": this.GlobalSettings.ITT = double.Parse(value[1]); break;
                    case "APPI Lamp": this.GlobalSettings.APPI = value[1].Trim(); break;
                    case "Flow Rate (uL/min)": this.GlobalSettings.SyringeFlow = double.Parse(value[1]); break;
                    case "Volume (uL)": this.GlobalSettings.SyringeVolume = double.Parse(value[1]); break;
                    case "Inner Diameter (mm)": this.GlobalSettings.SyringeDiameter = double.Parse(value[1]); break;
                    case "Pressure Mode": this.GlobalSettings.PressureMode = value[1].Trim(); break;
                    case "Default Charge State": this.GlobalSettings.DefaultCharge = int.Parse(value[1]); break;
                    case "Infusion Mode (LC)": this.GlobalSettings.IsLC = bool.Parse(value[1]); break;
                    case "FAIMS Mode": this.GlobalSettings.FAIMS = value[1].Trim(); break;
                    case "Application Mode": this.GlobalSettings.ApplicationMode = value[1].Trim(); break;
                    case "Advanced Peak Determination": this.GlobalSettings.APD = bool.Parse(value[1]); break;
                    case "Xcalibur AcquireX enabled for method modifications": this.GlobalSettings.AcquireX = bool.Parse(value[1]); break;
                    default:
                        { //can be comments or something unknown (will be ignored)
                            if (line.EndsWith("Method Summary")) this.Instrument = line.Substring(0, line.IndexOf("Method Summary") - 1).Trim(); //get Instrument Name
                            else if (line.StartsWith("Creator")) //parse Creator and Last Modified comment
                            {
                                this.Creator = line.Substring(line.IndexOf("Creator") + 9, line.IndexOf("Last Modified") - line.IndexOf("Creator") - 9).Trim();
                                this.LastModified = line.Substring(line.IndexOf("Last Modified") + 15).Trim();
                            }
                        } break;
                }
            }

            for (int i = 1; i < parts.Length; i++) //Experimnts start from part 1 (i.e. after first "Experiment")
            {
                Experiments.Add(new MSExperiment(parts[i])); 
            }
        }

        public static bool serialize(MSMethod method, string filename)
        {
            //serialize MS method as XML
            System.IO.TextWriter xmlStream = new System.IO.StreamWriter(filename);

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(MSMethod));
                serializer.Serialize(xmlStream, method);
                xmlStream.Close();
            }
            catch
            {
                return false;
            }
            finally
            {
                if (xmlStream != null)
                    ((IDisposable)xmlStream).Dispose();
            }
            return true;
        }

        static string repr(object that)
        {
            //Create string representation of an object
            //Handling collections recursively
            if (that == null) return String.Empty;
            if (that.GetType().GetInterface("IEnumerable") != null && !(that is string))
            {
                System.Collections.IEnumerable thatCollection = (System.Collections.IEnumerable)that;
                List<string> collectionRepr = new List<string>();
                foreach (var item in thatCollection)
                {
                    collectionRepr.Add(repr(item));
                }
                if (collectionRepr.Count > 0) return String.Format("\"{0}\"", String.Join(" ", collectionRepr));
                else return String.Empty; //empty collections represented as empty strings
            }
            else
            {
                return that.ToString();
            }
        }

        public IList<Dictionary<string, string>> prepareForCSV()
        {
            //Convert experiment list to be exported in CSV format as in ToCSV
            IList<Dictionary<string, string>> csvList = new List<Dictionary<string, string>>();

            foreach(MSExperiment experiment in this.Experiments)
            {
                Dictionary<string, string> csvLine = new Dictionary<string, string>();

                foreach (var property in experiment.GetType().GetFields()) //convert all fields to strings
                {
                    csvLine.Add(property.Name, repr(property.GetValue(experiment)));
                }

                csvList.Add(csvLine);
            }

            return csvList;
        }
    }
}
