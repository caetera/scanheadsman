﻿using System;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;
using ThermoFisher.CommonCore.Data.Interfaces;
using ThermoFisher.CommonCore.Data.Business;
using System.Globalization;
using Mono.Options;
using ThermoFisher.CommonCore.Data;

namespace ScanHeadsman
{
    public class ScanHeadsman
    {
        static private string selfPath = Assembly.GetExecutingAssembly().Location;
        static private string selfFileName = Path.GetFileNameWithoutExtension(selfPath);
        static private string selfName = Assembly.GetExecutingAssembly().GetName().Name;
        static private string selfVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();

        static public MSDetail detail = MSDetail.Header;
        static public bool summary = false;
        static public int maxThreads = Environment.ProcessorCount; //number of logical processors - optimized empirically

        static private bool recurse;

        private delegate void ExtractionMethod(IList<InstrumentMethod> methods, string rawfilename);
        static private ExtractionMethod extractMethods = null;

        static Regex TNGName = new Regex("Orbitrap\\s*(Fusion|Eclipse)\\s*(?:Lumos)?", RegexOptions.IgnoreCase);

        static public void extractAsText(IList<InstrumentMethod> methods, string rawFilename)
        {
            //export all methods as text
            StreamWriter methodfile = null;
            string methodFilename = Path.ChangeExtension(rawFilename, "methods.txt");

            Console.Out.WriteLine("Extracting method to {0}", methodFilename);

            try
            {
                methodfile = new StreamWriter(methodFilename, false, Encoding.Unicode);
            }
            catch
            {
                throw new IOException(String.Format("Can not create output file: {0}", methodFilename));
            }

            for (int i = 0; i < methods.Count; i++)
            {
                methodfile.WriteLine(String.Format("Method {0} is {1}\n\n{2}", i, methods[i].Name, methods[i].Text));
            }

            methodfile.Close();
        }

        static public void extractAsXML(IList<InstrumentMethod> methods, string rawFilename)
        {
            //extract MS method as XML
            string methodFilename = Path.ChangeExtension(rawFilename, "method.xml");

            Console.Out.WriteLine("Extracting MS method to {0}", methodFilename);

            for (int i = 0; i < methods.Count; i++)
            {
                if (TNGName.IsMatch(methods[i].Name)) // only TNG is supported so far
                {
                    MSMethod.serialize(new MSMethod(methods[i].Text), methodFilename);
                    break;
                }
                else
                {
                    Console.Out.WriteLine("Method from {0} is not supported", methods[i].Name);
                }
            }
        }

        static public void extractAsCSV(IList<InstrumentMethod> methods, string rawFilename)
        {
            //extract MS experiments as CSV
            string methodFilename = Path.ChangeExtension(rawFilename, "experiments.csv");

            Console.Out.WriteLine("Extracting experiments from MS method to {0}", methodFilename);

            for (int i = 0; i < methods.Count; i++)
            {
                if (TNGName.IsMatch(methods[i].Name)) // only TNG is supported so far
                {
                    MSMethod method = new MSMethod(methods[i].Text);
                    ToCSV(method.prepareForCSV(), methodFilename);
                    break;
                }
                else
                {
                    Console.Out.WriteLine("Method from {0} is not supported", methods[i].Name);
                }
            }
        }

        static public void ToCSV(IEnumerable<Dictionary<string, string>> data, string outputFilename, string columnSplitter = ",", string nan = "NA")
        {
            List<string> headers = new List<string>();
            int index = 0;
            Dictionary<string, Dictionary<int, string>> columns = new Dictionary<string, Dictionary<int, string>>();
            StreamWriter outfile = null;

            foreach (Dictionary<string, string> item in data)
            {
                foreach (KeyValuePair<string, string> kvpair in item)
                {
                    if (!headers.Contains(kvpair.Key))
                    {
                        headers.Add(kvpair.Key);
                        columns.Add(kvpair.Key, new Dictionary<int, string>());
                    }

                    columns[kvpair.Key][index] = kvpair.Value;
                }

                index++;
            }

            try
            {
                outfile = new StreamWriter(outputFilename, false, Encoding.ASCII);
            }
            catch
            {
                throw new IOException(String.Format("Can not create output file: {0}", outputFilename));
            }

            outfile.WriteLine(String.Join(columnSplitter, headers));

            for (int i = 0; i < index; i++)
            {
                List<string> row = new List<string>();
                foreach (string key in headers)
                {
                    try
                    {
                        columns[key][i] = columns[key][i].Replace('"', '\'');
                        if (columns[key][i].Contains(columnSplitter)) columns[key][i] = String.Format("\"{0}\"", columns[key][i]);
                        row.Add(columns[key][i]);
                    }
                    catch (KeyNotFoundException)
                    {
                        row.Add(nan);
                    }
                }
                outfile.WriteLine(String.Join(columnSplitter, row));
            }

            outfile.Close();
        }

        static public void MakeSummary(IRawDataPlus raw, string outputFile)
        {
            string summaryFilename = Path.ChangeExtension(outputFile, "sum.csv");
            Console.Out.WriteLine("Writing summary file to: {0}", summaryFilename);

            InstrumentData instrumentData = raw.GetInstrumentData();
            IFileHeader fileHeader = raw.FileHeader;
            SampleInformation sampleData = raw.SampleInformation;

            List<Dictionary<string, string>> summary = new List<Dictionary<string, string>>(1);
            
            summary.Add(new Dictionary<string, string>
                {
                ["CreatedDateUTC"] = fileHeader.CreationDate.ToUniversalTime().ToString(),
                ["ModifiedDateUTC"] = fileHeader.ModifiedDate.ToUniversalTime().ToString(),
                ["CreatedBy"] = fileHeader.WhoCreatedId,
                ["CreatedByName"] = fileHeader.WhoCreatedLogon,
                ["ModifiedBy"] = fileHeader.WhoModifiedId,
                ["ModifiedByName"] = fileHeader.WhoModifiedLogon,
                ["Description"] = fileHeader.FileDescription,
                ["InstrumentModel"] = instrumentData.Model,
                ["InstrumentName"] = instrumentData.Name,
                ["InstrumentSerialNumber"] = instrumentData.SerialNumber,
                ["InstrumentSoftwareVersion"] = instrumentData.SoftwareVersion,
                ["InstrumentHardwareVersion"] = instrumentData.HardwareVersion,
                ["InstrumentFlags"] = instrumentData.Flags,
                ["SampleBarcode"] = sampleData.Barcode,
                ["SampleBarcodeStatus"] = sampleData.BarcodeStatus.ToString(),
                ["CalibrationFile"] = sampleData.CalibrationFile,
                ["CalibrationLevel"] = sampleData.CalibrationLevel,
                ["SampleComment"] = sampleData.Comment,
                ["SampleDilutionFactor"] = sampleData.DilutionFactor.ToString(".000"),
                ["SampleInjectionVolume"] = sampleData.InjectionVolume.ToString(".00"),
                ["InstrumentMethod"] = sampleData.InstrumentMethodFile,
                ["SampleIstdAmount"] = sampleData.IstdAmount.ToString(".0000"),
                ["ProcessingMethod"] = sampleData.ProcessingMethodFile,
                ["SampeRowNumber"] = sampleData.RowNumber.ToString(),
                ["SampleID"] = sampleData.SampleId,
                ["SampleName"] = sampleData.SampleName,
                ["SampleType"] = sampleData.SampleType.ToString(),
                ["SampleVolume"] = sampleData.SampleVolume.ToString(".00"),
                ["SampleWeight"] = sampleData.SampleWeight.ToString(".00"),
                ["SampleVial"] = sampleData.Vial
            });

            for (int i = 0; i < sampleData.UserText.Length; i++)
            {
                summary[0].Add("UserText" + i.ToString(), sampleData.UserText[i]);
            }

            ToCSV(summary, summaryFilename);
        }

        static public void ProcessRawFile(string rawFilename, string outputFile)
        {
            //open raw file
            Console.Out.WriteLine("Processing of {0}", rawFilename);
            IRawFileThreadManager rawManager;
            IRawDataPlus raw0;

            try
            {
                 rawManager = RawFileReaderFactory.CreateThreadManager(rawFilename);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Cannot create ThreadManager from {0}\n{1}\n{2}", rawFilename, e.Message, e.StackTrace);
                return;
            }

            try
            {
                raw0 = rawManager.CreateThreadAccessor();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Cannot create ThreadAccessor from {0}\n{1}\n{2}", rawFilename, e.Message, e.StackTrace);
                rawManager.Dispose();
                return;
            }

            if (raw0.IsError)
            {
                Console.Error.WriteLine("RawFileThreadManager reported error opening {0}\n{1}", rawFilename, raw0.FileError.ErrorMessage);
                raw0.Dispose();
                rawManager.Dispose();
                return;
            }

            try
            {
                raw0.SelectInstrument(Device.MS, 1);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Cannot select MS instrument in {0}", rawFilename);
                Console.Error.WriteLine("Message: {0}\nTrace\n{1}", ex.Message, ex.StackTrace);
                raw0.Dispose();
                rawManager.Dispose();
                return;
            }

            //extract method information
            extractMethods?.Invoke(ThermoRawfileHelpers.GetMethods(raw0), outputFile);

            //Create summary
            if(summary) MakeSummary(raw0, outputFile);

            if (detail == MSDetail.None) return; //no MS output at all

            //Regexes for information parsing
            Regex TargetExpr = new Regex(@"(?<=T\=)\S+");
            Regex UfillExpr = new Regex(@"(?<=Ufill\=)\S+");
            Regex HCDEExpr = new Regex(@"(?<=HCD\=)\d+(?=eV)");
            Regex LMExpr = new Regex(@"Lock\(.*(\d)\/\d.*?(\S+)ppm\)");

            //collected data
            ConcurrentStack<Dictionary<string, string>> harvest = new ConcurrentStack<Dictionary<string, string>>();

            ParallelOptions options = new ParallelOptions
            {
                MaxDegreeOfParallelism = maxThreads
            };

            //Update scan count every second
            System.Timers.Timer tick = new System.Timers.Timer(1000);
            tick.Elapsed += (object sender, System.Timers.ElapsedEventArgs e) => Console.Out.Write("\rProcessing scan {0} of {1}", harvest.Count, raw0.RunHeader.LastSpectrum);
            tick.Start();

            //Process scans in parallel
            ParallelLoopResult res = Parallel.For(raw0.RunHeader.FirstSpectrum, raw0.RunHeader.LastSpectrum + 1, options, scan =>
                {
                    //for each thread we need to create accessor interface
                    IRawDataPlus raw = rawManager.CreateThreadAccessor();
                    raw.SelectInstrument(Device.MS, 1);

                    try
                    {
                        Dictionary<string, string> head = new Dictionary<string, string>();
                        head.Add("Scan", scan.ToString());
                        head.Add("MSOrder", ((int)raw.GetScanEventForScanNumber(scan).MSOrder).ToString());
                        head.Add("FilterString", raw.GetFilterForScanNumber(scan).ToString());
                        head.Add("DataType", raw.GetScanStatsForScanNumber(scan).IsCentroidScan ? "Centroid" : "Profile");

                        if (raw.GetScanEventForScanNumber(scan).MSOrder.CompareTo(ThermoFisher.CommonCore.Data.FilterEnums.MSOrderType.Ms) > 0)
                        {
                            IList<FragmentationEvent> events = ThermoRawfileHelpers.ExtractFragmentationData(head["FilterString"]);

                            for (int i = 0; i < events.Count; i++)
                            {
                                head[String.Format("SelectedMass{0}", i + 1)] = events[i].SelectedMass.ToString(".00");
                                head[String.Format("Activation{0}", i + 1)] = events[i].Activation;
                                head[String.Format("Energy{0}", i + 1)] = events[i].Energy.ToString(".00");
                            }

                        }

                        foreach (KeyValuePair<string, string> item in ThermoRawfileHelpers.ParseScanEvent(raw.GetScanEventStringForScanNumber(scan)))
                        {
                            head[item.Key] = item.Value;
                        }

                        ScanStatistics scanstats = raw.GetScanStatsForScanNumber(scan);
                        head["RT [min]"] = scanstats.StartTime.ToString(".00000");
                        head["LowMass"] = scanstats.LowMass.ToString(".00000");
                        head["HighMass"] = scanstats.HighMass.ToString(".00000");
                        head["TIC"] = scanstats.TIC.ToString(".0");
                        head["BasePeakPosition"] = scanstats.BasePeakMass.ToString(".00000");
                        head["BasePeakIntensity"] = scanstats.BasePeakIntensity.ToString(".0");

                        LogEntry trailer = raw.GetTrailerExtraInformation(scan);
                        for (int i = 0; i < trailer.Length; i++)
                        {
                            head[trailer.Labels[i].Trim(new char[] { ' ', ':', '\t' })] = trailer.Values[i].Trim();
                        }

                        //squize some information from older instruments
                        if (head.ContainsKey("FT Analyzer Settings"))
                        {
                            head["TargetAGC"] = TargetExpr.Match(head["FT Analyzer Settings"]).Value;
                        }

                        if (head.ContainsKey("FT Analyzer Message"))
                        {
                            string ufillString = UfillExpr.Match(head["FT Analyzer Message"]).Value;
                            head["Underfill"] = (ufillString == "") ? "1.0" : ufillString;
                            string HCDEString = HCDEExpr.Match(head["FT Analyzer Message"]).Value;
                            head["HCD Energy [eV]"] = (HCDEString == "") ? "0.0" : HCDEString;
                            Match LMString = LMExpr.Match(head["FT Analyzer Message"]);
                            head["NrLockMasses"] = LMString.Groups[1].Value;
                            head["LockMassDeviation [ppm]"] = LMString.Groups[2].Value;
                        }

                        //Mass Data
                        Scan msscan = Scan.FromFile(raw, scan);
                        double[] masses;
                        double[] intensities;

                        if (msscan.HasCentroidStream)
                        {
                            masses = msscan.CentroidScan.Masses;
                            intensities = msscan.CentroidScan.Intensities;
                        }
                        else
                        {
                            Scan centroidedScan = head["DataType"] == "Profile" ? Scan.ToCentroid(msscan) : msscan;

                            masses = centroidedScan.SegmentedScan.Positions;
                            intensities = centroidedScan.SegmentedScan.Intensities;

                        }

                        head.Add("NrCentroids", masses.Length.ToString());

                        if (detail == MSDetail.Small)
                        {
                            if (masses.Length > 0) //non-empty spectra
                            {
                                head.Add("minInt", intensities.Min().ToString(".0"));
                                head.Add("maxInt", intensities.Max().ToString(".0"));
                                head.Add("meanInt", (intensities.Sum() / intensities.Length).ToString(".0"));
                            }
                            else
                            {
                                head.Add("minInt", "0.0");
                                head.Add("maxInt", "0.0");
                                head.Add("meanInt", "0.0");
                            }
                        }
                        else if (detail == MSDetail.Full)
                        {
                            head.Add("mzArray", String.Format("\"{0}\"", String.Join(" ", masses.Select(x => x.ToString(".00000")))));
                            head.Add("intArray", String.Format("\"{0}\"", String.Join(" ", intensities.Select(x => x.ToString(".0")))));
                        }

                        //fix some weird stuff from trialer info
                        if (head["MSOrder"] == "1")
                        {
                            head["Monoisotopic M/Z"] = "";
                            head["Charge State"] = "";
                        }

                        harvest.Push(head);
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine("Error processing scan {0} from {1}", scan, rawFilename);
                        Console.Error.WriteLine("Message:{0}\nTrace\n{1}", ex.Message, ex.StackTrace);
                    }

                    raw.Dispose(); //dispose accessor
                }
            );

            
            if (res.IsCompleted)
            {
                tick.Stop();
                Console.Out.WriteLine("\rScan processing done; Total: {0} scans\nWriting to CSV file.", harvest.Count);
                ToCSV(harvest.OrderBy(head => int.Parse(head["Scan"])), outputFile);
                Console.Out.WriteLine("Done");
            }

            //dispose all, close file, clear storage
            raw0.Dispose();
            harvest.Clear();
            rawManager.Dispose();
        }

        static int ProcessFolder(string input, string output)
        {
            int numfiles = 0;
            foreach (string rawfileName in Directory.GetFiles(input, "*.raw"))
            {
                try
                {
                    ProcessRawFile(rawfileName, Path.Combine(output, Path.GetFileNameWithoutExtension(rawfileName) + ".csv"));
                    numfiles++;
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Error processing {0}\nMessage:{1}\nTrace\n{2}", rawfileName,
                        ex.Message, ex.StackTrace);
                }
            }

            if (recurse)
            {
                foreach (string folderName in Directory.GetDirectories(input))
                {
                    Console.Out.WriteLine("Enter {0}", folderName);
                    numfiles += ProcessFolder(folderName, output);
                }
            }

            return numfiles;
        }

        static string GetValidMSLevels()
        {
            List<string> output = new List<string>();
            foreach(int v in Enum.GetValues(typeof(MSDetail)))
            {
                output.Add(String.Format("{0} ({1})", Enum.GetName(typeof(MSDetail), v), v));
            }

            return String.Join("\n", output);
        }

        static CmdArgs ParseCLI(string[] cliArgs)
        {
            bool showHelp = false;
            bool showVersion = false;
            CmdArgs args = new CmdArgs();
            OptionSet options = new OptionSet
            {
                { String.Format("Usage\n{0} [option arguments] [positional arguments]\n" + 
                                "All arguments are optional, input and output can be provided either " +
                                "as option arguments or as positional arguments\nOptions:", selfFileName) },
                { "h|help", "Usage information", _ =>  showHelp = true },
                { "v|version", "Show version information", _ => showVersion = true },
                { "i|input=", "Input file/folder",  v => args.input = v},
                { "o|output=", "Output file/folder", v => args.output = v },
                { "d|detail=", "MS data detail level", v => args.detail = v },
                { "m|methods:", "Extract methods", v => args.methods = v = v ?? "" }, //handle empty modifier
                { "u|summary", "Create raw file summary (Creator ID, Creation Date, Instrument Model ...)", _ => args.summary = true },
                { "t|threads=", "Number of parallel threads, has to be positive interger or -1 (no limit)", (int? v) => args.maxthreads = v ?? 0 },
                { "r|recurse", "Recursive folder processing", _ => recurse = true }
            };

            List<string> positionArgs = new List<string>();

            try
            {
                positionArgs = options.Parse(cliArgs);
            }
            catch (OptionException e)
            {
                Console.Error.WriteLine(String.Format("Error parsing command line:\n{0}", e.Message));
                Environment.Exit(-1);
            }

            if(showHelp)
            {
                options.WriteOptionDescriptions(Console.Out);
                Environment.Exit(0);
            }

            if(showVersion)
            {
                Console.Out.WriteLine("{0} Version {1}", selfName, selfVersion);
                Environment.Exit(0);
            }

            if (positionArgs.Count > 0 && (!args.input.IsNullOrEmpty() || !args.output.IsNullOrEmpty()))
            {
                Console.Error.WriteLine("Error parsing command line:\nInput and output are defined both as positional and option arguments");
                Environment.Exit(-1);
            }
            else
            {
                switch (positionArgs.Count)
                {
                    case 0:
                        break;
                    case 1:
                        args.input = positionArgs[0];
                        args.output = "";
                        break;
                    default:
                        args.input = positionArgs[0];
                        args.output = positionArgs[1];
                        break;
                }
            }

            if (args.methods != null)
            {
                switch (args.methods)
                {
                    case "xml": extractMethods = extractAsXML; break;
                    case "csv": extractMethods = extractAsCSV; break;
                    case "text": extractMethods = extractAsText; break;
                    default: extractMethods = extractAsText; break;
                }
            }

            if (args.summary != null) summary = true;

            if (args.detail != null)
            {
                if (int.TryParse(args.detail, out var detailInt)) //detail string is parsable as int
                {
                    if (Enum.IsDefined(typeof(MSDetail), detailInt)) //int corresponds to valid enum value
                    {
                        detail = (MSDetail)detailInt;
                    }
                    else
                    {
                        Console.Error.WriteLine("Invalid MS detail level - {0}\nValid values are:\n{1}", args.detail, GetValidMSLevels());
                        Environment.Exit(-1);
                    }
                }
                else
                {
                    try
                    {
                        detail = (MSDetail)Enum.Parse(typeof(MSDetail), args.detail, true);
                    }
                    catch (Exception)
                    {
                        Console.Error.WriteLine("Cannot parse MS detail argument from '{0}'\nValid values are:\n{1}", args.detail, GetValidMSLevels());
                        Environment.Exit(-1);
                    }

                }
            }

            if (args.maxthreads != null)
            {
                if (args.maxthreads < 1 && args.maxthreads != -1)
                {
                    Console.Out.WriteLine("WARNING: incorrect value for maxthread - '{0}', it has to be positive or -1 (no limitation)", args.maxthreads);
                    Console.Out.WriteLine("Using default value: {0}", maxThreads);
                }
                else
                {
                    maxThreads = args.maxthreads.Value;
                }
            }

            return args;
        }

        static void Main(string[] args)
        {
            int numFiles = 0;

            //override number and regional formats
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;

            CmdArgs cliArgs = ParseCLI(args);

            if (cliArgs.input.IsNullOrEmpty() && cliArgs.output.IsNullOrEmpty())
            {
                Console.Out.WriteLine("No input/output arguments provided\nLooking for raw files in current folder\n" +
                    "Using default extension for result");
                numFiles += ProcessFolder(Directory.GetCurrentDirectory(), Directory.GetCurrentDirectory());
            }

            if (!cliArgs.input.IsNullOrEmpty() && cliArgs.output.IsNullOrEmpty())
            { 
                if (Directory.Exists(cliArgs.input))
                {
                    Console.Out.WriteLine("The input argument is a folder\nLooking for raw files in it\nUsing default extension for result");
                    numFiles += ProcessFolder(cliArgs.input, cliArgs.input);
                }
                else if (File.Exists(cliArgs.input))
                {
                    Console.Out.WriteLine("The argument is a filename\nUsing default output name: {0}", Path.ChangeExtension(cliArgs.input, "csv"));
                    ProcessRawFile(cliArgs.input, Path.ChangeExtension(cliArgs.input, "csv"));
                    numFiles++;
                }
                else
                {
                    Console.Error.WriteLine(String.Format("Can not find input path: {0}", cliArgs.input));
                    Environment.Exit(-1);
                }
            }

            if (!cliArgs.input.IsNullOrEmpty() && !cliArgs.output.IsNullOrEmpty())
            {
                if (File.Exists(cliArgs.input))
                {
                    string outputFolder = Path.GetDirectoryName(cliArgs.output);
                    if (!Directory.Exists(outputFolder))
                    {
                        try
                        {
                            Directory.CreateDirectory(outputFolder);
                        }
                        catch (Exception ex)
                        {
                            Console.Error.WriteLine("Error creating folder {0}\n", outputFolder, ex.Message);
                            Environment.Exit(-1);
                        }
                    }
                    
                    ProcessRawFile(cliArgs.input, cliArgs.output);
                    
                }
                else if (Directory.Exists(cliArgs.input))
                {
                    Console.Out.WriteLine("The argument is a folder\nLooking for raw files in it\nUsing default extension for result");
                    if (!Directory.Exists(cliArgs.output))
                    {
                        try
                        {
                            Directory.CreateDirectory(cliArgs.output);
                        }
                        catch (Exception ex)
                        {
                            Console.Error.WriteLine("Error creating folder {0}\n", cliArgs.output, ex.Message);
                            Environment.Exit(-1);
                        }
                    }

                    numFiles += ProcessFolder(cliArgs.input, cliArgs.output);
                }
                else
                {
                    Console.Error.WriteLine(String.Format("First argument should be valid file or folder name; {0} received", cliArgs.input));
                    Environment.Exit(-1);
                }
            }
            Console.Out.WriteLine("{0} files processed", numFiles);
        }
    }
    
}
